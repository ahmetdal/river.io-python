__author__ = 'ahmetdal'

from setuptools import setup, find_packages

try:
    long_description = open('README.md').read()
except IOError:
    long_description = ''

setup(
    name='river.io-python',
    version='0.0.1',
    author='Ahmet DAL',
    author_email='ceahmetdal@gmail.com',
    packages=find_packages(),
    url='https://github.com/javrasya/river.io-python.git',
    description='Python client for River.IO',
    long_description=long_description,
    install_requires=[
        "httplib2"
    ],
    include_package_data=True,
    zip_safe=False,
    license='GPLv3',
    platforms=['any'],
    test_suite="river_io_python.tests"
)
