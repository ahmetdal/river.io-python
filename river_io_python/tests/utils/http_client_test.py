from river_io_python.utils.http_client import HttpClient

__author__ = 'ahmetdal'

import ast
from httplib import OK
import json
import threading
from unittest import TestCase


__author__ = 'ahmetdal'


class TestClient(TestCase):
    def setUp(self):
        from wsgiref.validate import validator
        from wsgiref.simple_server import make_server

        self.return_value_with_get = "Test Result Get"
        self.return_value_with_post = "Test Result Post"
        self.return_value_with_put = "Test Result Put"
        self.return_value_with_delete = "Test Result Delete"
        self.data = {'test': 'test'}

        def simple_app(environ, start_response):
            try:
                request_body_size = int(environ.get('CONTENT_LENGTH', 0))
            except ValueError:
                request_body_size = 0

            request_body = environ['wsgi.input'].read(request_body_size)

            rd = {}
            if request_body:
                d = json.loads(request_body)
                for k, v in d.iteritems():
                    if isinstance(v, list):
                        rd[k] = v[0] if len(v) > 0 else None
                    else:
                        rd[k] = v

            status = '200 OK'  # HTTP Status
            headers = [('Content-type', 'text/plain')]
            start_response(status, headers)
            if environ['REQUEST_METHOD'] == 'GET':
                return [self.return_value_with_get]
            if environ['REQUEST_METHOD'] == 'POST':
                return ['%s.%s' % (self.return_value_with_post, rd)]

            if environ['REQUEST_METHOD'] == 'PUT':
                return ['%s.%s' % (self.return_value_with_put, rd)]

            if environ['REQUEST_METHOD'] == 'DELETE':
                return [self.return_value_with_delete]

        validator_app = validator(simple_app)

        self.httpd = make_server('', 0, validator_app)
        print "Listening on port %s...." % self.httpd.server_port
        t = threading.Thread(target=self.httpd.serve_forever)
        t.daemon = True
        t.start()


    def test_proxy_with_get(self):
        response, content = HttpClient.proxy('http://%s:%s/' % self.httpd.server_address, 'GET')
        self.assertEqual(OK, response.status)
        self.assertEqual(self.return_value_with_get, content)


    def test_proxy_with_post(self):
        response, content = HttpClient.proxy('http://%s:%s/' % self.httpd.server_address, 'POST', data=self.data)
        self.assertEqual(OK, response.status)
        parts = content.split('.')
        self.assertEqual(self.return_value_with_post, parts[0])
        self.assertDictEqual(self.data, ast.literal_eval(parts[1]))

    def test_proxy_with_put(self):
        response, content = HttpClient.proxy('http://%s:%s/' % self.httpd.server_address, 'PUT', data=self.data)
        self.assertEqual(OK, response.status)
        parts = content.split('.')
        self.assertEqual(self.return_value_with_put, parts[0])
        self.assertDictEqual(self.data, ast.literal_eval(parts[1]))


    def test_proxy_with_delete(self):
        response, content = HttpClient.proxy('http://%s:%s/' % self.httpd.server_address, 'DELETE')
        self.assertEqual(OK, response.status)
        self.assertEqual(self.return_value_with_delete, content)


    def tearDown(self):
        self.httpd.shutdown()
