from functools import wraps
from httplib import OK
import json
from urllib import urlencode

from src.exceptions import RiverIOClientException


__author__ = 'ahmetdal'


def river_io_client(func=None):
    @wraps(func)
    def decorated(request, *args, **kwargs):
        data = kwargs.get('data', None)
        if data:
            kwargs['data'] = urlencode(data)
        result, content = func(request, *args, **kwargs)
        if result.status == OK:
            return json.loads(content)
        else:
            raise RiverIOClientException(content)

    return decorated