import json
import httplib2

__author__ = 'ahmetdal'


class HttpClient:
    @classmethod
    def get_connection(cls):
        return httplib2.Http()


    @classmethod
    def proxy(cls, url, method, data=None, headers=None):
        data = json.dumps(data) if data is not None else data
        return HttpClient.get_connection().request(url, method, body=data, headers=headers)
