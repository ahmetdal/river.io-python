__author__ = 'ahmetdal'

OBJECT_COUNT_WAITING_FOR_APPROVAL_URL = 'object_count_waiting_for_approval/%(content_type_id)s/%(field_id)s/%(user_id)s/'
OBJECTS_WAITING_FOR_APPROVAL_URL = 'objects_waiting_for_approval/%(content_type_id)s/%(field_id)s/%(user_id)s/'
IS_USER_AUTHORIZED_URL = 'is_user_authorized/%(content_type_id)s/%(field_id)s/%(user_id)s/'
STATE_BY_ID_URL = 'state/%(pk)s/'
FILTER_STATE_URL = 'state/?(filter_by)s'
REGISTER_OBJECT_URL = 'register_object/'
PROCESSES_TRANSITION__URL = 'processes_transition/'