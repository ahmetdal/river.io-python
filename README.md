# River.IO Python

This contains Python rest client of <a href='https://bitbucket.org/ahmetdal/river.io'>RiverIO</a>


## Installation

```	
pip install git+https://ahmetdal@bitbucket.org/ahmetdal/river.io-python.git
```
	

## Usage
This will provide client code of RiverIO RESTfull services. Here is the client code usage;


```python
from river_io_python.rest_client import *
client=RiverIORestClient('river_io_server_base_url/web/','YOUR_RIVER_IO_AUTH_TOKEN')
```
Here are some functionalites that the client provides;

* `client.get_on_approval_object_count(<content_type_id>,<field_id>,<user_id>)`,`returns integer value`
* `client.get_on_approval_objects(<content_type_id>,<field_id>,<user_id>)`,`returns objects as list`
* `client.is_user_authorized(<content_type_id>,<field_id>,<user_id>)`,`returns boolean value`
* `client.register_object(<content_type_id>,<object_id>,<field_id>)`
* `client.process_transition(<process_type[0,1]>,<content_type_id>,<object_id>,<field_id>,<?next_state_id>)`

